Categories:Time
License:Apache2
Web Site:
Source Code:https://github.com/Kestutis-Z/World-Weather
Issue Tracker:https://github.com/Kestutis-Z/World-Weather/issues

Auto Name:World Weather
Summary:View weather forecast
Description:
View the forecast from OpenWeatherMap. Two kinds of weather forecast are
available: 14-day daily weather forecast, and three-hourly forecast for up to 5
days.
.

Repo Type:git
Repo:https://github.com/Kestutis-Z/World-Weather

Build:1.1.1,15
    commit=37a90fb46cb5fd99fc634a1ab325e1bd7b296765
    subdir=WorldWeather/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.1.1
Current Version Code:15

