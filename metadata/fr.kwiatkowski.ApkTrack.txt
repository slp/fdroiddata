Categories:System
License:GPLv3
Web Site:https://github.com/JusticeRage/ApkTrack/blob/HEAD/README.md
Source Code:https://github.com/JusticeRage/ApkTrack
Issue Tracker:https://github.com/JusticeRage/ApkTrack/issues
Bitcoin:19wFVDUWhrjRe3rPCsokhcf1w9Stj3Sr6K

Auto Name:ApkTrack
Summary:Check for updates on PlayStore
Description:
Periodically checks if your installed apps can be updated.

It was created for users who don't use the Google Play Store, but still need to
know when new APKs are available for their apps. ApkTrack performs simple
website scraping to grab the latest versions of packages present on the device.

Note that the update checks don't care whether you are using 3G, 4G or WiFi.
Please do not install it if your mobile plan charges data at a premium. Also,
you have to manually refresh you app list from time to time.
.

Repo Type:git
Repo:https://github.com/JusticeRage/ApkTrack

Build:1.0,1
    commit=3804c965808d87737595034a7f377a0915a69bb2

Build:1.0a,2
    commit=1.0a

Build:1.0b,3
    commit=1.0b

Build:1.0c,4
    commit=1.0c

Build:1.1,6
    commit=1.1

Build:1.1e,8
    commit=1.1e

Build:1.1f,9
    commit=1.1f

Build:1.1g,10
    commit=1.1g

Build:1.1h,11
    commit=v1.1h

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1h
Current Version Code:11

