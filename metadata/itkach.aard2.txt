Categories:Reading
License:GPLv3
Web Site:http://aarddict.org
Source Code:https://github.com/itkach/aard2-android
Issue Tracker:https://github.com/itkach/aard2-android/issues

Auto Name:Aard 2
Summary:Offline dictionary
Description:
Aard 2 for Android is a successor to [[aarddict.android]]. It comes with
redesigned user interface, bookmarks, history and a better dictionary storage
format.
.

Repo Type:git
Repo:https://github.com/itkach/aard2-android.git

Build:0.26,26
    disable=java8
    commit=0.26
    init=rm libs/android-support-v13.jar
    gradle=yes
    srclibs=slobber@0.9,slobj@0.7
    prebuild=cp -fR $$slobber$$ ../ && \
        cp -fR $$slobj$$ ../ && \
        sed -i -e '/support-v13/acompile "com.android.support:support-v13:22.2.0"' -e 's|compile files|//compile files|g' build.gradle
    target=android-19

Maintainer Notes:
* slobj has a jar file
* throws error (java8) on slobj
* settings.gradle uses includeflat, which points to ../projectname (see cp -fR).
.

Auto Update Mode:None
#Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.27
Current Version Code:27

