Categories:Multimedia
License:GPLv2+
Web Site:https://github.com/Fortyseven/ToneDef/blob/HEAD/README.md
Source Code:https://github.com/Fortyseven/ToneDef
Issue Tracker:https://github.com/Fortyseven/ToneDef/issues

Auto Name:ToneDef
Summary:Tone dialer with DTMF, bluebox and redbox tones
Description:
Small, but powerful tone dialer application featuring DTMF, bluebox, and redbox
tone generation. Use the keypad, enter a predefined sequence, or select an entry
from your contact list.
.

Repo Type:git
Repo:https://github.com/Fortyseven/ToneDef

Build:11,11
    disable=https://github.com/Fortyseven/ToneDef/issues/2
    commit=11
    subdir=MainApp
    gradle=yes

Auto Update Mode:None
#Auto Update Mode:Version %c
Update Check Mode:Tags
Current Version:11
Current Version Code:11

