Categories:Science & Education
License:GPLv3
Web Site:http://olejon.github.io/mdapp
Source Code:https://github.com/olejon/mdapp
Issue Tracker:https://github.com/olejon/mdapp/issues
Donate:http://www.olejon.net/code/mdapp/?page=donate

Auto Name:LegeAppen
Summary:Toolkit for medical personnel
Description:
Useful and intuitive app for doctors and other health care workers in Norway,
optimized for both phones and tablets. Be aware that this app is in Norwegian
only.

For a list of features, please visit the [https://olejon.github.io/mdapp/
website].

Required Permissions:

* In-app purchases: To be able to donate money to the developer
* Location: To be able to find your position in relation to a pharmacy
* Photos/Media/Files: To be able to store files with content that is presented in the app
* Camera: To be able to scan barcodes
* Wi-Fi connection information: To be able to determine if the device is connected to Wi-Fi

Your position is not logged in any way.

Disclaimer:

LegeAppen is developed by a private person, and even though the content in the
app is fetched from recognized sources, the developer can not the be held
responsible for decisions taken based on information found in this app.

Credits:

* Stethoscrope icon from Icons8: [http://icons8.com/web-app/957/Stethoscope]
.

Repo Type:git
#Repo:https://github.com/olejon/mdapp
Repo:https://github.com/olejon/mdapp-fdroid

Build:1.1,110
    disable=builds, legal issues
    commit=45f68902deb7f8d830a930d3713c1866a52caa23
    subdir=mdapp/app
    gradle=yes
    prebuild=pushd src/main/assets && \
        wget -c http://www.olejon.net/code/mdapp/api/1/felleskatalogen/db/felleskatalogen.full.db.zip -O felleskatalogen.db.zip && \
        popd

Build:1.2,120
    disable=builds, legal issues
    commit=5090c3eb5ac0ee59bd24bd67a08768d30e194bb7
    subdir=mdapp/app
    gradle=yes
    prebuild=pushd src/main/assets && \
        wget -c http://www.olejon.net/code/mdapp/api/1/felleskatalogen/db/felleskatalogen.full.db.zip -O felleskatalogen.db.zip && \
        popd

Build:1.4,143
    commit=73793efc4d574e790d4802d28efd488c4c573ad5
    subdir=mdapp/app
    gradle=yes

Build:1.5,150
    commit=6048742ef0d666dd22ec243ff952b80c4b4aa824
    subdir=mdapp/app
    gradle=yes

Build:1.6,160
    commit=29475d43a718bea68d054f18ae03648c788ee3a5
    subdir=mdapp/app
    gradle=yes

Build:1.7,172
    commit=eb5034c9ad9f5530118d674802785a174d9b2cbe
    subdir=mdapp/app
    gradle=yes

Build:1.8,180
    commit=d7c05066b738256b4f575bb8b5224d93d43dca84
    subdir=mdapp/app
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.8
Current Version Code:180

